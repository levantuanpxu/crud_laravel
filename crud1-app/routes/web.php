<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'user', 'middleware' =>[ 'auth']], function () {

    Route::get('', [App\Http\Controllers\UserController::class, 'index'])->name('users.index')->middleware('check.permission:user-list');
    Route::get('create', [App\Http\Controllers\UserController::class, 'create'])->name('users.create')->middleware('check.permission:user-create');
    Route::post('store', [App\Http\Controllers\UserController::class, 'store'])->name('users.store')->middleware('check.permission:user-create');
    Route::get('edit/{id}', [App\Http\Controllers\UserController::class, 'edit'])->name('users.edit')->middleware('check.permission:user-edit');
    Route::put('update/{id}', [App\Http\Controllers\UserController::class, 'update'])->name('users.update')->middleware('check.permission:user-edit');
    Route::delete('delete/{id}', [App\Http\Controllers\UserController::class, 'destroy'])->name('users.destroy')->middleware('check.permission:user-delete');
});
Route::group(['prefix' => 'role', 'middleware' =>'auth'], function () {

    Route::get('', [App\Http\Controllers\RoleController::class, 'index'])->name('roles.index')->middleware('check.permission:role-list');
    Route::get('create', [App\Http\Controllers\RoleController::class, 'create'])->name('roles.create')->middleware('check.permission:role-create');
    Route::post('store', [App\Http\Controllers\RoleController::class, 'store'])->name('roles.store')->middleware('check.permission:role-create');
    Route::get('edit/{id}', [App\Http\Controllers\RoleController::class, 'edit'])->name('roles.edit')->middleware('check.permission:role-edit');
    Route::put('update/{id}', [App\Http\Controllers\RoleController::class, 'update'])->name('roles.update')->middleware('check.permission:role-edit');
    Route::delete('delete/{id}', [App\Http\Controllers\RoleController::class, 'destroy'])->name('roles.destroy')->middleware('check.permission:role-delete');
});
Route::group(['prefix' => 'category', 'middleware' => 'auth'], function () {

    Route::get('', [App\Http\Controllers\CategoryController::class, 'index'])->name('categories.index')->middleware('check.permission:category-list');
    Route::get('create', [App\Http\Controllers\CategoryController::class, 'create'])->name('categories.create')->middleware('check.permission:category-create');
    Route::post('store', [App\Http\Controllers\CategoryController::class, 'store'])->name('categories.store')->middleware('check.permission:category-create');
    Route::get('edit/{id}', [App\Http\Controllers\CategoryController::class, 'edit'])->name('categories.edit')->middleware('check.permission:category-edit');
    Route::put('update/{id}', [App\Http\Controllers\CategoryController::class, 'update'])->name('categories.update')->middleware('check.permission:category-edit');
    Route::delete('delete/{id}', [App\Http\Controllers\CategoryController::class, 'destroy'])->name('categories.destroy')->middleware('check.permission:category-delete');
});
Route::group(['prefix' => 'product', 'middleware' => 'auth'], function () {

    Route::get('', [App\Http\Controllers\ProductController::class, 'index'])->name('products.index')->middleware('check.permission:product-list');
    Route::get('create', [App\Http\Controllers\ProductController::class, 'create'])->name('products.create')->middleware('check.permission:product-create');
    Route::post('store', [App\Http\Controllers\ProductController::class, 'store'])->name('products.store')->middleware('check.permission:product-create');
    Route::get('edit/{id}', [App\Http\Controllers\ProductController::class, 'edit'])->name('products.edit')->middleware('check.permission:product-edit');
    Route::post('update/{id}', [App\Http\Controllers\ProductController::class, 'update'])->name('products.update')->middleware('check.permission:product-edit');
    Route::get('delete/{id}', [App\Http\Controllers\ProductController::class, 'destroy'])->name('products.destroy')->middleware('check.permission:product-delete');
});
